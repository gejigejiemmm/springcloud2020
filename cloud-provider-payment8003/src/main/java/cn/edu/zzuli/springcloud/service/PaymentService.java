package cn.edu.zzuli.springcloud.service;

import cn.edu.zzuli.springcloud.entities.Payment;

public interface PaymentService {

    int create(Payment payment);

    Payment getPaymentById(Long id);
}
