package cn.edu.zzuli.springcloud.controller;

import cn.edu.zzuli.springcloud.service.PaymentHystrixService;
import com.netflix.hystrix.contrib.javanica.annotation.DefaultProperties;
import com.netflix.hystrix.contrib.javanica.annotation.HystrixCommand;
import com.netflix.hystrix.contrib.javanica.annotation.HystrixProperty;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;

@RestController
@DefaultProperties(defaultFallback = "paymentInfo_global_FallbackMethod")
public class OrderHystrixController {

    @Resource
    PaymentHystrixService paymentHystrixService;

    @GetMapping("/consumer/peyment/hystrix/ok/{id}")
    public String paymentInfo_ok(@PathVariable("id") Integer id) {
        return paymentHystrixService.paymentInfo_ok(id);
    }

    //如果超过 0.5秒自动降级，前去执行fallbackMethod
    //所以说，hystrix 我们一般用在客户端，也就是消费端，当执行超过预期时间的话，我们可以直接返回给前端系统繁忙，这不是美滋滋。
//    @HystrixCommand(fallbackMethod = "paymentInfo_timeoutHandler", commandProperties = {
//            @HystrixProperty(name = "execution.isolation.thread.timeoutInMilliseconds", value = "500")
//    })
    @HystrixCommand //如果只是这样，不指明自己的 fallbackMethod，会自动去找全局的fallbackMethod
    @GetMapping("/consumer/peyment/hystrix/timeout/{id}")
    public String paymentInfo_timeout(@PathVariable("id") Integer id) {
        return paymentHystrixService.paymentInfo_timeout(id);
    }

    public String paymentInfo_timeoutHandler(Integer id) {
        return "线程池： " + Thread.currentThread().getName() + "目标系统报错或者繁忙, (╯‵□′)╯︵┻━┻：）  id: " + id;
    }


    //下面是全局fallback
    public String paymentInfo_global_FallbackMethod() {
        return "来自全局fallback的提示：目标系统报错或者繁忙, (╯‵□′)╯︵┻━┻：）";
    }

}
