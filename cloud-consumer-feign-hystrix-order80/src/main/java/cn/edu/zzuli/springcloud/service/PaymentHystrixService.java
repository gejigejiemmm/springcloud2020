package cn.edu.zzuli.springcloud.service;

import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;

//fallback = PaymentFallbackService.class 如果该接口每个类都需要去特地的写一个fallback方法，那么我们可以新建一个实现类，
//用 fallback 属性，指定这个实现类，如果目标服务宕机或者异常，那么会自动去调用实现类中的方法
//不过经过测试，该方法 和 全局配置相比，（全局配置+@HystrixCommand） 优先级好像更高一点。
@FeignClient(value = "CLOUD-PROVIDER-HYSTRIX-PAYMENT",fallback = PaymentFallbackService.class)
public interface PaymentHystrixService {

    @GetMapping("/payment/hystrix/ok/{id}")
    public String paymentInfo_ok(@PathVariable("id") Integer id);

    @GetMapping("/payment/hystrix/timeout/{id}")
    public String paymentInfo_timeout(@PathVariable("id") Integer id);
}
