package cn.edu.zzuli.springcloud.service;

import org.springframework.stereotype.Component;

@Component
public class PaymentFallbackService implements PaymentHystrixService {
    @Override
    public String paymentInfo_ok(Integer id) {
        return "PaymentFallbackService_paymentInfo_ok";
    }

    @Override
    public String paymentInfo_timeout(Integer id) {
        return "PaymentFallbackService_paymentInfo_timeout";
    }
}
