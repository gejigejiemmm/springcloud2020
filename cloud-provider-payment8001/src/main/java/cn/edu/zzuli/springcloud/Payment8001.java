package cn.edu.zzuli.springcloud;

import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;
import org.springframework.cloud.netflix.eureka.EnableEurekaClient;

@SpringBootApplication
@MapperScan("cn.edu.zzuli.springcloud.dao")
@EnableEurekaClient //表示这是一个 eureka的客户端
@EnableDiscoveryClient //开启 discoveryClient，以便获取注册中心的 服务节点信息
public class Payment8001 {

    public static void main(String[] args) {
        SpringApplication.run(Payment8001.class, args);
    }

}
