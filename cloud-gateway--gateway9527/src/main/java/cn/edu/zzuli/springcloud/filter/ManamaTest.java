package cn.edu.zzuli.springcloud.filter;

import java.util.concurrent.TimeUnit;

public class ManamaTest {
    public static  void main(String[] args) {
        Thread t = new Thread() {
            public void run() {
                try {
                    TimeUnit.SECONDS.sleep(2);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
                System.out.println("t");
                Sogou();
            }
        };

        Thread t1 = new Thread() {
            public void run() {
                System.out.println("t1");
                Sogou();
            }
        };
        t.start();
        try {
            TimeUnit.MILLISECONDS.sleep(10);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        t1.start();
    }
    static synchronized void Sogou(){
        System.out.println("Sogou");
    }
}

