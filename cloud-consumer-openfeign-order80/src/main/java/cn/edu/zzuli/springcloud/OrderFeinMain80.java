package cn.edu.zzuli.springcloud;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.openfeign.EnableFeignClients;

@SpringBootApplication
@EnableFeignClients //开启feign客户端
public class OrderFeinMain80 {

    public static void main(String[] args) {
        SpringApplication.run(OrderFeinMain80.class, args);
    }

}
