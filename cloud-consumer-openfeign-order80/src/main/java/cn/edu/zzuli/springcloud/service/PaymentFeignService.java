package cn.edu.zzuli.springcloud.service;

import cn.edu.zzuli.springcloud.entities.CommonResult;
import cn.edu.zzuli.springcloud.entities.Payment;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.stereotype.Component;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;

//@Component,并不需要写component，注入容器，FeignClient会帮我们注入
@FeignClient(value = "CLOUD-PAYMENT-SERVICE")
public interface PaymentFeignService {

    //这里是会直接跳转到 CLOUD-PAYMENT-SERVICE/payment/get/{id}
    //而且OpenFeign底层也导入了 ribbion，默认带有负载均衡的。
    //但是它有超时控制，默认为1秒，超过1秒则报错。可在yml中进行配置
    @GetMapping("/payment/get/{id}")
    public CommonResult<Payment> getPaymentById(@PathVariable("id") Long id);

    @GetMapping("/payment/timeout")
    public String getPaymentTimeout();

}
