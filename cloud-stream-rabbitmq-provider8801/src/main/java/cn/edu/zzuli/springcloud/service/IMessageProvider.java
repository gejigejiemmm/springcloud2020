package cn.edu.zzuli.springcloud.service;

public interface IMessageProvider {

    public String send();

}
