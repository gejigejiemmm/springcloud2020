package cn.edu.zzuli.config;

import org.springframework.cloud.client.loadbalancer.LoadBalanced;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.client.RestTemplate;

@Configuration
public class ApplicationContextConfig {

    @Bean
//    @LoadBalanced //配置负载均衡策略,默认是轮询
    public RestTemplate restTemplate() {
        return new RestTemplate();
    }

}
